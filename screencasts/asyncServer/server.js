/**
 * Created by andrew on 26.06.15.
 */
var http = require('http');
//var fs = require('fs');
var sf = require('./sf');
/*
this example shows that single-threaded nodejs ap is async, but still single-threaded,
so delayed execution of a request will slow down whole application
 */
var server = http.createServer();
server.on('request', function (req, res) {
    if (req.url == '/') {
       new sf().readFile('index.html', function (err, info) {
            if (err) {
                console.log(err);
                res.statusCode = 500;
                res.end('ServerError');
            }
            res.end(info);
        });
    }

});
server.listen(3000);