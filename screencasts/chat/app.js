var express = require('express')
    , routes = require('routes');
var config = require("config");
var log = require("libs/log")(module);
var HttpError = require("error/HttpError").HttpError;
var mongoose = require("mongoose");
var app = express();

app.engine("ejs", require("ejs-locals"));
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

if (app.get("env") == "development") {
    app.use(express.logger("dev"));
} else {
    app.use(express.logger("default"));
}
app.use(express.bodyParser());

app.use(express.cookieParser());

var MongoSession = require("connect-mongo")(express);
app.use(express.session({
    secret: config.get("session:secret"),
    //key: config.get("session:key"),
    cookie: config.get("session:cookie"),
    store: new MongoSession({mongooseConnection: mongoose.connection})
}));
/*
app.use(function(req, res, next){
    if (req.session.numberOfVisits)
        req.session.numberOfVisits = req.session.numberOfVisits + 1;
    else
        req.session.numberOfVisits = 1;
    res.json({visits: req.session.numberOfVisits});
});*/

app.use(express.methodOverride());

app.use(require("middelware/sendHttpError"));
app.use(require("middelware/loadUser"));
app.use(app.router);

require("routes")(app);

app.use(express.static(__dirname + '/public'));

app.use(function (err, req, res, next) {
    if (typeof err == "number") {
        err = new HttpError(err);
    }
    if (err instanceof HttpError) {
        res.sendHttpError(err);
    } else {
        if (app.get("env") == "developent") {
            express.errorHandler()(err, req, res, next);
        } else {
            log.error(err);
            err = new HttpError(500);
            res.sendHttpError(err);
        }
    }
});

app.listen(config.get("port"), function () {
    log.info("Express server listening on port " + config.get("port") + " in " + app.settings.env + " mode");
});
