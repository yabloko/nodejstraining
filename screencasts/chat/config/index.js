/**
 * Created by andrew on 15.07.15.
 */
var fs = require("fs");
var nconf = require("nconf");
var path = require("path");

nconf.argv().env().file({file: path.join(__dirname, "config.json")});

module.exports = nconf;