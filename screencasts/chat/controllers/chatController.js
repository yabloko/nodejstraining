/**
 * Created by andrew on 29.07.15.
 */
exports.get = function(req, res, next){
    res.render("chat", {userName: req.user.userName});
}