/**
 * Created by andrew on 29.07.15.
 */
var HttpError = require("error/HttpError").HttpError;
var AuthError = require("error/AuthError");
var User = require("models/user").User;
var ObjectID = require("mongodb").ObjectID;

var log = require("libs/log")(module);

exports.loginGet= function (req, res, next) {
    res.render("login");
}

exports.loginPost = function (req, res, next) {
    var username = req.body.username;
    var password = req.body.password;
    User.authorize(username, password, function (err, user) {
        if (err) {
            if (err instanceof AuthError) {
                return next(new HttpError(403, err.message));
            } else {
                return next(err);
            }
        }
        req.session.userId = user._id;
        res.send({});
    });

}