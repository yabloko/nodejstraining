/**
 * Created by andrew on 29.07.15.
 */
var HttpError = require("error/HttpError").HttpError;
var User = require("models/user").User;
var ObjectID = require("mongodb").ObjectID;

exports.list = function (req, res, next) {
    User.find({}, function (err, users) {
        if (err) return next(err);
        res.json(users);
    })
};

exports.get = function (req, res, next) {
    try {
        var id = new ObjectID(req.params.id);
    }
    catch (err) {
        return next(err);
    }
    User.findById(id, function (err, user) {
        if (err) return next(err);
        if (!user) {
            next(new HttpError(404, "user not found"));
        } else {
            res.json(user);
        }
    })
};

exports.delete = function (req, res, next) {
    try {
        var id = new ObjectID(req.params.id);
    }
    catch (err) {
        return next(500);
    }
    User.findById(id, function (err, user) {
        if (err) return next(err);
        if (!user) {
            next(new HttpError(404, "user not found"));
        } else {
            user.remove(function(err, user){
                if(err) return next(err);
                res.end();
            });
        }
    })
};