var mongoose = require("./libs/mongoose");
var async = require("async");

function open(callback) {
    mongoose.connection.on("open", callback);
}

function dropDatabase(callback) {
    mongoose.connection.db.dropDatabase(callback);
}

function requireModels(callback) {
    require("./models/user");
    async.each(Object.keys(mongoose.models), function (modelName, callback) {
        mongoose.models[modelName].ensureIndexes(callback);
    }, callback);
}

function createUsers(callback) {

    var users = [{userName: "John", password: "123"},
        {userName: "Jack", password: "123"},
        {userName: "admin", password: "123"}];
    async.each(users, function (userData, callback) {
        var item = new mongoose.models.User(userData);
        item.save(callback);
    }, callback);

}

async.series([open,
    dropDatabase,
    requireModels,
    createUsers], function (err, result) {
    if (err) throw err;
    mongoose.disconnect();
    console.log(arguments);
});