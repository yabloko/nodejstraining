/**
 * Created by andrew on 31.07.15.
 */
var log = require("./log")(module);
var HttpError = require("../error/HttpError").HttpError;

exports.executeIfAuthorized = function (role, req, res, next, callback) {
    if(role === true) {
        callback(req, res, next);
        return;
    }
    //should be altered to more sophisticated permission matrix
    //all logged in users belong to logged_in group
    if(role === "logged_in" && req.user){
        log.warn("user " + req.user.userName + " is authorized as " + role);
        callback(req, res, next);
        return;
    }
    if (req.user && req.user.userName === role) {
        log.warn("user " + req.user.userName + " is authorized as " + role);
        callback(req, res, next);
        return;
    }
    else {
        next(new HttpError(403, "user is not authorized to perform this action"));
    }
}