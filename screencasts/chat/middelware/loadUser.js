/**
 * Created by andrew on 30.07.15.
 */
var User = require("models/user").User;

module.exports = function (req, res, next) {
    if(!req.session.userId) return next();

    var user = User.findById(req.session.userId, function(err, user){
        if(err){
            log.error(err);
            return next(err);
        }
        req.user = user;
        next();
    });

}