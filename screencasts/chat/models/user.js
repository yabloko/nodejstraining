/**
 * Created by andrew on 17.07.15.
 */
var crypto = require("crypto");
var mongoose = require("../libs/mongoose"), Schema = mongoose.Schema;
var AuthError = require("error/AuthError");
var async = require("async");

var schema = new Schema({
    userName: {type: String, unique: true, required: true},
    passwordHash: {type: String, required: true},
    salt: {type: String, required: true},
    created: {type: Date, default: Date.now}
});

schema.methods.encryptPassword = function (password) {
    return crypto.createHmac("sha1", this.salt).update(password).digest("hex");
}

schema.methods.checkPassword = function(password) {
    return this.encryptPassword(password) === this.passwordHash;
}

schema.statics.authorize = function(username, password, callback) {
    var User = this;
    async.waterfall([
            function (callback) {
                User.findOne({userName: username}, callback);
            },
            function (user, callback) {
                if (user) {
                    if (user.checkPassword(password)) {
                        callback(null, user);
                    } else {
                        callback(new AuthError("Incorrect password"));
                    }
                } else {
                    user = new User({userName: username, password: password});
                    user.save(function (err) {
                        if (err) {
                            log.error(err);
                            callback(err);
                        }
                        callback(null, user);
                    });
                }
            }
        ],
        callback);
}

schema.virtual("password").set(function (password) {
    this._plainPassword = password;
    this.salt = Math.random() + '';
    this.passwordHash = this.encryptPassword(password);
}).get(function () {
    return this._plainPassword;
});

exports.User = mongoose.model("User", schema);