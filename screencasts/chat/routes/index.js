var authManager = require("../libs/authManager");

module.exports = function (app) {
    app.get("/", function (req, res, next) {
        authManager.executeIfAuthorized(true, req, res, next, require("../controllers/frontpageController").get);
    });

    app.get("/users", function (req, res, next) {
        authManager.executeIfAuthorized("admin", req, res, next, require("../controllers/userController").list);
    });

    app.get("/user/:id", function (req, res, next) {
        authManager.executeIfAuthorized("admin", req, res, next, require("../controllers/userController").get);
    });

    app.delete("/user/:id", function (req, res, next) {
        authManager.executeIfAuthorized("admin", req, res, next, require("../controllers/userController").delete);
    });

    app.get("/login", function (req, res, next) {
        authManager.executeIfAuthorized(true, req, res, next, require("../controllers/loginController").loginGet);
    });

    app.post("/login", function (req, res, next) {
        authManager.executeIfAuthorized(true, req, res, next, require("../controllers/loginController").loginPost);
    });

    app.get("/chat", function (req, res, next) {
        authManager.executeIfAuthorized("logged_in", req, res, next, require("../controllers/chatController").get);
    });
};