/**
 * Created by andrew on 13.07.15.
 */

var http = require("http");
var HashMap = require("hashmap");

var config = new HashMap();
var args = process.argv.slice(2);

args.map(function (val) {
     var pair = val.split("=");
    config.set(pair[0], pair[1]);
});


http.createServer(function (req, res) {
    res.end("i am alive");
}).listen(config.get("port"));