/**
 * Created by andrew on 10.07.15.
 */
var fs = require("fs");
var url = require("url");

exports.handler = function (req, res) {
    var parsedUrl = url.parse(req.url);
    var fPath = "." + parsedUrl.pathname;
   console.log("i am handler, path is " + fPath);


    fs.exists(fPath, function (exists) {
        if (exists) {
            var fStream = fs.createReadStream(fPath);
            res.on("close", function(){
                fStream.destroy();
            });
            fStream.pipe(res);
            res.statusCode = 202;
        }
        else {
            res.statusCode = 404;
            res.end("Not Found");
        }
    });

};