/**
 * Created by andrew on 10.07.15.
 */

log = console.log;
console.log = function (msg) {
    log("[" + new Date().toISOString() + "]" + msg);
};
error = console.error;
console.error = function (msg) {
    error("[" + new Date().toISOString() + "]" + msg);
};
var domain = require("domain");

var serverDomain = domain.create();

var server;

serverDomain.on("error", function (err) {
    console.error("error - " + err);
});

serverDomain.run(function () {
    var http = require("http");
    var handler = require("./handler");
    server = http.createServer(function (req, res) {
        var reqDomain = domain.create();

        reqDomain.add(req);
        reqDomain.add(res);
        reqDomain.on("error", function(err){
            res.statusCode = 500;
            res.end("server error");
            serverDomain.emit("error", err);
        });

        reqDomain.run(function(){
            handler.handler(req, res);
        });

    }).listen(3000);
});