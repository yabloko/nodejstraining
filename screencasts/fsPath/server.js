/**
 * Created by andrew on 30.06.15.
 */
var http = require('http');
var fs = require('fs');
var url = require('url');
var path = require('path');

var ROOT = __dirname + "/public";

var server = http.createServer(function (req, res) {
    if (!checkAccess(req)) {
        res.statusCode = 403;
        res.end("Secret not accepted");
        return;
    }
    sendFileSafe(url.parse(req.url).pathname, res);
}).listen(3000);

function checkAccess(req) {
    return url.parse(req.url, true).query.secret == "scrt";
}

function sendFileSafe(fPath, res) {
    try {
        fPath = decodeURIComponent(fPath);
        if (~fPath.indexOf("\0"))
            throw new Error("zero-byte is not allowed in path");
        fPath = path.normalize(path.join(ROOT, fPath));
        if (fPath.indexOf(ROOT) != 0)
            throw new Error("requested path " + fPath.toString() + " is invalid");
    }
    catch (e) {
        console.error(e.message);
        res.statusCode = 400;
        res.end("bad request");
        return;
    }

    fs.stat(fPath, function (err, stats) {
        if (err) {
            console.error(err.message);
            return;
        }
        if (!stats.isFile()) {
            res.statusCode = 404;
            res.end("requested resource is not a file");
            return;
        }
    });
    sendFile(fPath, res);
}

function sendFile(fPath, res) {
    fs.readFile(fPath, function(err, data){
       if(err){
           console.error(err.message);
           res.statusCode = 503;
           res.end("server error");
           return;
       }
        var mime = require('mime').lookup(fPath);
        res.setHeader('Content-type', mime + "; charset=utf-8");
        res.end(data);
    });
}