var User = require('./user/index');
var db = require('db');
var log = require('logger')(module);
//db.connect();

function run() {
    var petya = new User("Petya");
    var vasya = new User("Vasya");
    vasya.hello(petya);
    log(db.getPhrase("run_successfull"));
}

if (module.parent) {
    exports.run = run;
}
else {
    run();
}