/**
 * Created by andrew on 19.06.15.
 */

var http = require('http');
var url = require('url');
var log = require('./log')(module);
var server = new http.Server(function (req, res) {
        log.info(req.headers);
        log.info(req.method, req.url);
        var urlParsed = url.parse(req.url, true);
        log.info(urlParsed);
        if (urlParsed.pathname == '/echo' && urlParsed.query.message) {
            res.setHeader('Cache-control', 'no-cache');
            res.end(urlParsed.query.message);
        }
        else {
            res.statusCode = 404;
            log.error("Paege nit fun");
            res.end("Paege nit fun");
        }
    })
    ;

server.listen(1337, '127.0.0.1');

var emit = server.emit;
server.emit = function (event) {
    log.debug(event);
    emit.apply(server, arguments);
}


/*
 server.on('request', function(req, res){
 res.end("Hleo wrdol" + ++counter);
 });
 */
