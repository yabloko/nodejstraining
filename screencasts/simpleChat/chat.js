/**
 * Created by andrew on 08.07.15.
 */
var clients = [];

exports.subscribe = function (req, res) {
    console.log(" - subscribed");
    res.on("close", function () {
        clients.slice(clients.indexOf(res), 1);
    });
    clients.push(res);
    console.log(clients.length);
    return null;
};

exports.publish = function (message) {
    clients.forEach(function (r) {
        console.log(" - publishing message for client" + r.toString());
        r.end(message);
    });
    clients = [];
};