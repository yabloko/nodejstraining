/**
 * Created by andrew on 08.07.15.
 */

csl = console.log;
console.log = function (msg) {
    csl("[" + new Date().toISOString() + "]" + msg);
}

var http = require("http");
var fs = require("fs");
var chat = require("./chat");

http.createServer(function (req, res) {
    switch (req.url) {
        case "/":
            sendFile("index.html", res);
            break;
        case "/subscribe":
            chat.subscribe(req, res);
            break;
        case "/publish":
            var body = "";
            req.on("readable", function () {
                body += req.read();
                if(body.length > 1e4){
                    res.statusCode = 413;
                    res.end("Message too long");
                }
            }).on("end", function () {
                try {
                    body = JSON.parse(body);
                }
                catch (err) {
                    console.error(err.getMessage());
                    res.statusCode = 400;
                    res.end("Bad request");
                    return;
                }
                chat.publish(body.message);
                res.end("ok");
            });

            break;
        default:
            res.statusCode = 404;
            res.end("Not Found");
    }
}).listen(3000);

function sendFile(fileName, res) {
    var fStream = fs.createReadStream(fileName);
    fStream.on("error", function (err) {
        console.error(err.toString());
        res.statusCode = 500;
        res.end("Internal Server Error");
    }).pipe(res);
}