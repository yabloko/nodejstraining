/**
 * Created by andrew on 02.07.15.
 */
var http = require("http");
var fs = require("fs");
var path = "/home/andrew/PB240306.JPG";

new http.Server(function (req, res) {
    if (req.url == '/picture') {
        fs.createReadStream(path).pipe(res);
        res.on("close", function () {
            console.log("destroyed");
        })
    } else {
        res.statusCode = 404;
        res.end("not found");
    }
}).listen(3000);

function sendPipe(inStream, outStream) {
    inStream.pipe(outStream);

    inStream.on("error", function (err) {
        outStream.statusCode = 500;
        outStream.end("internal server error");
        console.error(err.message);
    });

    inStream.on("open", function () {
        console.log("opened");
    });
    inStream.on("close", function () {
        console.log("closed");
    });

   outStream.on("close", function () {
       console.log("destroyed");
        inStream.destroy();
    });
}

function sendFile(fStream, outStream) {
    fStream.on("readable", write);

    function write() {
        var data = fStream.read();
        /* if data is present and content is delivered fast - outStream.write() will return true, buffer is empty
         * and if block will not be executed, but if buffer is full, we temporary remove readable event listener
         * and wait for current data to be transfered, then add listener again
         */
        if (data && !outStream.write(data)) {
            fStream.removeListener("readable", write);
            outStream.once("drain", function () {
                fStream.on("readable", write);
                write();
            });
        }
    }

    fStream.on("end", function () {
        outStream.end();
    });
}