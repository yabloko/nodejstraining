/**
 * Created by andrew on 29.06.15.
 */
var fs = require('fs');

//some action taken - open this file
fs.open(__filename, "r", function(err, file){
    console.log("io");
});

//executed AFTER file is opened
setImmediate(function(){
    console.log("Immediate");
});

//executed BEFORE file is opened
process.nextTick(function(){
    console.log("next tick");
});