/**
 * Created by andrew on 29.06.15.
 */
var http = require('http');

var server = http.createServer(function (req, res) {

}).listen(3000);

/*terminates server after 2.5 sec
 */
setTimeout(function () {
    server.close();
}, 2500);

/*logs memory consumption every 1 sec
 */
var timer = setInterval(function () {
    console.log(process.memoryUsage());
}, 1000);

/*this line tells nodejs that timer will not *hold* code execution
 *and server will be terminated by setTimeout
 */
timer.unref();