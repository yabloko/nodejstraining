/**
 * Created by andrew on 17.06.15.
 */
var util = require('util');

function HttpError(status, message) {
    this.status = status;
    this.message = message;
    Error.captureStackTrace(this, HttpError);
}
util.inherits(HttpError, Error);
HttpError.prototype.name = "HttpError";

module.exports = HttpError;