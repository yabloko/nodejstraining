var util = require('util');
var PhraseError = require('./error/PhraseError');
var HttpError = require('./error/HttpError');
var phrases = {
    "hello": "privet",
    "world": "mir"
};

function getPhrase(name) {
    if (!phrases[name]) {
        throw new PhraseError("phrase not found");
    }
    return phrases[name];
}

function makePage(url) {
    if (url != "index.html") {
        throw new HttpError(404, "page not found");
    }
    return util.format("%s %s!", getPhrase("hell"), getPhrase("world"));
}

try {
    var page = makePage("index.html");
    console.log(page);
}
catch (e) {
    if (e instanceof HttpError) {
        console.log(e.status, e.message);
    }
    else {
        console.error("Error: %s\nMesage: %s\nTrace: %s\n", e.name, e.message, e.stack);
    }
}