/**
 * Created by andrew on 18.06.15.
 */
var EventEmitter = require('events').EventEmitter;

var db = new EventEmitter();

function Request() {
    var self = this;

    this.bigData = new Array(10e6).join('*');

    this.send = function (data) {
        console.log(data);
    };

    function onData(info) {
        self.send(info);
    }

    this.onData = onData;

    this.end = function () {
        db.removeListener('data', onData)
    }

    db.on('data', onData);
}

setInterval(function () {
    var request = new Request();
    request.onData("infoa");
    //db.emit('data', "infoe");
    request.end();
    console.log(process.memoryUsage().heapUsed);
    //console.log(db);

}, 200);