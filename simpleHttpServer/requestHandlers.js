var exec = require("child_process").exec;
var querystring = require("querystring");

function index(response, postData) {
	console.log("request handles index was called");
	response.writeHead(200, {"Content-Type": "text/plain"});
        response.write("Hello Index");
        response.end();
}

function start(response, postData) {
	console.log("Request handler 'start' was called.");
	exec("ls -lah", function (error, stdout, stderr) {
                response.writeHead(200, {"Content-Type": "text/plain"});
                response.write(stdout);
                response.end();
        });
}

function upload(response, postData) {
	console.log("Request handler 'upload' was called.");
	response.writeHead(200, {"Content-Type": "text/plain"});
	response.write("post request data was:" + 
	JSON.stringify(querystring.parse(postData))
	);
	response.end();
}

exports.start = start;
exports.upload = upload;
exports.index = index;
