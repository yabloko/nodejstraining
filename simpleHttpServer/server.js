var http = require("http");
var url = require("url");

function start(route, handle) {
	function onRequest(request, response) {
		var postData = "";
		var pathName = url.parse(request.url).pathname;
		console.log("request url is " + pathName);
	
		request.setEncoding("utf8");

		request.addListener("data", function(postDataChunk) {
			postData += postDataChunk;
			console.log("post data chunk recieved: " + postDataChunk);
		});

		request.addListener("end", function() {
			route(handle, pathName, response, postData);
		});
	}
	http.createServer(onRequest).listen(8888);
	console.log("server started");
}
exports.start = start;
