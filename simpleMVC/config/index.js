/**
 * Created by andrew on 11.08.15.
 */
var config = {
    local: {mode: "local", port: 3000, db: {uri: "mongodb://localhost:27017/fastdelivery"}},
    staging: {mode: "staging", port: 4000, db: {uri: "mongodb://localhost:27017/fastdelivery"}},
    production: {mode: "production", port: 5000, db: {uri: "mongodb://localhost:27017/fastdelivery"}}
}

module.exports = function (mode) {
    return config[mode || "local"];
}