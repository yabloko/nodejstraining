/**
 * Created by andrew on 11.08.15.
 */
var config = require("../../config")();

describe("Database connection", function () {
    it("is there mongodb server running", function (next) {
        var mongoClient = require("mongodb").MongoClient;
        mongoClient.connect(config["db"]["uri"], function (err, db) {
            expect(err).toBe(null);
            expect(db).toBeDefined();
            db.close();
            next();
        });
    });
});
